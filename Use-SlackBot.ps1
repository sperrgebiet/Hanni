﻿[CmdletBinding()]
param
(
)

$token = "YourSlackApiToken"


function Connect-Slack
{
    param(
        $apiUrl = "https://slack.com/api/rtm.start",
        [Parameter(Mandatory=$true)]
        $token
    )
    $body = @{ token = $token }
    $apiRequest = Invoke-RestMethod -Uri $apiUrl -Method Post -Body $body

    #[System.Net.WebSockets.ClientWebSocket]$ws = [System.Net.WebSockets.ClientWebSocket]::new() # Doesn't work with PS4...
    [System.Net.WebSockets.ClientWebSocket]$ws = New-Object System.Net.WebSockets.ClientWebSocket
    [void]$ws.ConnectAsync($apiRequest.url, [System.Threading.CancellationToken]::None)

    while($ws.State -ne "Open")
    {
        Start-Sleep -Milliseconds 500
    }
    Write-Verbose -Message "$(Get-TimeStamp)`tConnected. Wuhu"

    return $ws
}

function Get-SlackMessage
{
    param(
        [Parameter(Mandatory=$true)]
        [System.Net.WebSockets.ClientWebSocket]$ws
    )

    [System.ArraySegment[byte]]$receiveBuffer = New-Object Byte[] 4096
    $wsResult = $ws.ReceiveAsync($receiveBuffer, [System.Threading.CancellationToken]::None)

    return ([System.Text.Encoding]::UTF8.GetString($receiveBuffer.Array, 0, $wsResult.Result.Count)) | ConvertFrom-Json
}

function Send-SlackMessage
{
    param(
        [Parameter(Mandatory=$true)]
        [System.Net.WebSockets.ClientWebSocket]$ws,
        [Parameter(Mandatory=$true)]
        [string]$message,
        [string]$room = "C0Y3ZKL0M"
    )
    $text = New-Object PSCustomObject
    Add-Member -InputObject $text -MemberType NoteProperty -Name "type"    -Value "message"
    Add-Member -InputObject $text -MemberType NoteProperty -Name "channel" -Value $room
    Add-Member -InputObject $text -MemberType NoteProperty -Name "text"    -Value $message

    $json = $text | ConvertTo-Json
    $jsonBytes = ([System.Text.Encoding]::UTF8.GetBytes($json))

    [void]$ws.SendAsync($jsonBytes, [System.Net.WebSockets.WebSocketMessageType]::Text, $true, [System.Threading.CancellationToken]::None)
    if($ws.IsCompleted -eq $true)
    {
        Write-Verbose "$(Get-TimeStamp)`tSend Message => Room: $room`tMessage:$message"
    }

}

function Send-SlackPing
{
    param(
        [Parameter(Mandatory=$true)]
        [System.Net.WebSockets.ClientWebSocket]$ws
    )
    $textObj = New-Object PSCustomObject
    Add-Member -InputObject $textObj -MemberType NoteProperty -Name "type" -Value "ping"
    $json = ConvertTo-Json -InputObject $textObj
    $jsonBytes = ([System.Text.Encoding]::UTF8.GetBytes($json))
    [void]$ws.SendAsync($jsonBytes, [System.Net.WebSockets.WebSocketMessageType]::Text, $true, [System.Threading.CancellationToken]::None)
    if($ws.IsCompleted -eq $true)
    {
        return 0
    }
    else
    {
        return $ws.Exception
    }
}

function Get-TimeStamp
{
    return (Get-Date -Format "MM/dd/yyyy-hh:mm:ss").ToString()
}

function Get-FortuneCookie
{
    param(
        [string]$category
    )
    If($category.length -gt 0)
    {
        $cookie = Invoke-RestMethod -Uri http://fortunecookies.cloudapp.net/Cookies.svc/$category/json
    }
    Else
    {
        $cookie = Invoke-RestMethod -Uri http://fortunecookies.cloudapp.net/Cookies.svc/cookie/json
    }
    
    return $cookie
}

#
# End of functions
#


$ws = Connect-Slack -token $token

while($true)
{
    Start-Sleep -Milliseconds 500
    $msg = Get-SlackMessage -ws $ws

    Write-Verbose "$(Get-TimeStamp)`tLastMessageType: $($msg.type)"

    If($msg.type -eq "message")
    {
        Write-Verbose "$(Get-TimeStamp)`tMessage received: $($msg.text)"
        switch ($msg.text)
        {
            {$_ -like "*Hans*"   } { Send-SlackMessage -ws $ws -message "Ich hab den Hans ganz fest lieb" }
            {$_ -like "*Philipp*"} { Send-SlackMessage -ws $ws -message "Der Philipp is schon auch OK..." }
            {$_ -like "*Hanni*"  } { Send-SlackMessage -ws $ws -message "Das bin iiiiiiiiich"             }
            {$_ -like "*Hanni, was kannst du so*"  } { Send-SlackMessage -ws $ws -message "Ich bin noch jung und unerfahren. Willst du mir was beibringen?"             }
            {$_ -like "*Hanni, läufst du auf Azure*"  } { Send-SlackMessage -ws $ws -message "Nope, momentan leb ich auf ner VM in Seattle, da isses auch flauschig"    }
            {$_ -like "*Hanni, gib mir nen Keks*"  } { Send-SlackMessage -ws $ws -message (Get-FortuneCookie)    }
            Default {}
        }
    }


}



Send-Ping $ws